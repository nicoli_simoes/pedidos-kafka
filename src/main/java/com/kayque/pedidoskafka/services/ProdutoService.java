package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Produto;

import java.util.List;

public interface ProdutoService {

    List<Produto> listarTodos();

    Produto cadastrar(Produto produto);
}
