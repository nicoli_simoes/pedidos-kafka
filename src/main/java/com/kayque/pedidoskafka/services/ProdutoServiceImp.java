package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Produto;
import com.kayque.pedidoskafka.repositories.ProdutoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProdutoServiceImp implements ProdutoService{

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    public List<Produto> listarTodos(){
        return produtoRepository.findAll();
    }

    @Override
    public Produto cadastrar(Produto produto){
        return produtoRepository.save(produto);
    }

}
