package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Cliente;
import com.kayque.pedidoskafka.entities.Pedido;

import java.util.List;

public interface ClienteService {
    List<Cliente> listar();
    Cliente cadastrar(Cliente cliente);
    Pedido buscarPorId(String codigo);
}
