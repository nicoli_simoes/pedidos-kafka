package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Cliente;
import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.repositories.ClienteRepository;
import com.kayque.pedidoskafka.repositories.PedidoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements ClienteService {
    private final ClienteRepository clienteRepository;
    private final PedidoRepository pedidoRepository;

    @Override
    public List<Cliente> listar() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente cadastrar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public Pedido buscarPorId(String codigo){
        return pedidoRepository.findByCodigo(codigo)
                .orElseThrow(()->new IllegalArgumentException("Pedido não encontrado!"));
    }
}
