package com.kayque.pedidoskafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PedidosKafkaApplication {
	public static void main(String[] args) {
		SpringApplication.run(PedidosKafkaApplication.class, args);
	}

}
