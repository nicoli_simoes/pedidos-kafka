package com.kayque.pedidoskafka.kafka;

import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.repositories.PedidoRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@RequiredArgsConstructor
public class PedidoKafkaConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(PedidoKafkaConsumer.class);
    private final PedidoRepository pedidoRepository;

    private Pedido payload;

    @KafkaListener(topics = "${spring.kafka.topic-json.name}", groupId = "${spring.kafka.consumer.group-id}")
    public void consume(Pedido pedido){
        this.payload=pedido;
        LOGGER.info(String.format("Pedido recebido -> %s", payload));
        pedidoRepository.save(pedido);
    }
}
