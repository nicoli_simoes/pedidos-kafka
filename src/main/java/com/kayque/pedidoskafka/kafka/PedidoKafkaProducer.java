package com.kayque.pedidoskafka.kafka;

import com.kayque.pedidoskafka.entities.Pedido;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PedidoKafkaProducer {

    @Value("${spring.kafka.topic-json.name}")
    private String topicJsonName;

    private static final Logger LOGGER = LoggerFactory.getLogger(PedidoKafkaProducer.class);

    private final KafkaTemplate<String, Pedido> kafkaTemplate;

    public void sendMessage(Pedido pedido){
        LOGGER.info(String.format("Pedido enviado -> %s", pedido.toString()));

        Message<Pedido> message = MessageBuilder
                .withPayload(pedido)
                .setHeader(KafkaHeaders.TOPIC, topicJsonName)
                .build();

        kafkaTemplate.send(message);
    }
}
