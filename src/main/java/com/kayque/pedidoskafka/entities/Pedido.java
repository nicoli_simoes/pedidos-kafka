package com.kayque.pedidoskafka.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "pedidos")
public class Pedido {

    @Id
    private String codigo;
    @DBRef
    private Cliente cliente;
    @DBRef
    private List<Produto> produtos;
}
