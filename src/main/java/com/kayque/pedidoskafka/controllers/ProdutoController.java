package com.kayque.pedidoskafka.controllers;

import com.kayque.pedidoskafka.entities.Produto;
import com.kayque.pedidoskafka.services.ProdutoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor// assim pode usar a classe produto service como final para deixar mais limpo
@RequestMapping("/produtos")
public class ProdutoController {
    private final ProdutoService produtoService;

    @GetMapping
    public List<Produto> listarTodos(){
        List<Produto> lista= produtoService.listarTodos();
        return ResponseEntity.ok().body(lista).getBody();
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Produto> cadastrar(@RequestBody Produto produto){
//        produto = produtoService.cadastrar(produto);
        URI uri= ServletUriComponentsBuilder
                     .fromCurrentRequest().path("/{_id}")
                     .buildAndExpand(produto.get_id()).toUri();
        return ResponseEntity.created(uri).body(produtoService.cadastrar(produto));
    }


}
