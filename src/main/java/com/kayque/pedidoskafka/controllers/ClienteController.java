package com.kayque.pedidoskafka.controllers;

import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.kafka.PedidoKafkaProducer;
import com.kayque.pedidoskafka.services.ClienteService;
import com.kayque.pedidoskafka.entities.Cliente;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/clientes")
public class ClienteController {
    private final ClienteService clienteService;
    private final PedidoKafkaProducer pedidoKafkaProducer;

    @GetMapping("/listar")
    public ResponseEntity<List<Cliente>> listar(){
        return ResponseEntity.ok(clienteService.listar());
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Cliente> cadastrar(@RequestBody Cliente cliente) throws URISyntaxException {
      return  ResponseEntity.created(new URI(String.format("/clientes/%s", cliente.getCodigo())))
              .body(clienteService.cadastrar(cliente));
    }
    @PostMapping("/fazerPedido")
    public ResponseEntity<Pedido> fazerPedido(@RequestBody Pedido pedido) throws URISyntaxException {
        pedidoKafkaProducer.sendMessage(pedido);
        return ResponseEntity.created(new URI(String.format("/pedidos/%s", pedido.getCodigo())))
                .body(pedido);
    }

    @GetMapping("/buscaPedido/{codigo}")
    public ResponseEntity<Pedido> buscarPedidoPorCodigo(@PathVariable(value = "codigo") String codigo){
        return ResponseEntity.ok(clienteService.buscarPorId(codigo));
    }
}
