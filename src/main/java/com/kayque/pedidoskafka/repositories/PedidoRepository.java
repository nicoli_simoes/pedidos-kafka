package com.kayque.pedidoskafka.repositories;

import com.kayque.pedidoskafka.entities.Pedido;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PedidoRepository extends MongoRepository<Pedido,String> {
    Optional<Pedido> findByCodigo(String codigo);
}
