package com.kayque.pedidoskafka.repositories;

import com.kayque.pedidoskafka.entities.Produto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProdutoRepository extends MongoRepository<Produto, String> {
}
