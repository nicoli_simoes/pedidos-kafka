package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Produto;
import com.kayque.pedidoskafka.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ProdutoServiceTest {

    @Autowired
    private ProdutoServiceImp produtoServiceImp;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Test
    public void deveListarTodosProdutos(){
        List<Produto> produtos= Arrays.asList(
                Produto.builder()
                        .nome("Banana")
                        .build(),
                Produto.builder()
                        .nome("Arroz")
                        .build()
        );

        Mockito.when(produtoRepository.findAll())
                .thenReturn(produtos);

        Assertions.assertEquals(produtos,produtoServiceImp.listarTodos());

        Mockito.verify(produtoRepository).findAll();

    }

    @Test
    public void DeveCadastrarProduto(){
        var produto = Produto.builder().nome("Laranja").build();

        Mockito.when(produtoRepository.save(produto)).thenReturn(produto);

        Assertions.assertEquals(produto,produtoServiceImp.cadastrar(produto));

        Mockito.verify(produtoRepository).save(produto);
    }
}
