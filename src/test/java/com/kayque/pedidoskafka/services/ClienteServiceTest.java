package com.kayque.pedidoskafka.services;

import com.kayque.pedidoskafka.entities.Cliente;
import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.repositories.ClienteRepository;
import com.kayque.pedidoskafka.repositories.PedidoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ClienteServiceTest {

    @Autowired
    private ClienteServiceImpl clienteService;

    @MockBean
    private ClienteRepository clienteRepository;
    @MockBean
    private PedidoRepository pedidoRepository;

    @Test
    public void deveListarClientes(){

        List<Cliente> clientes= Arrays.asList(
                Cliente.builder()
                        .nome("abc")
                        .build(),
                Cliente.builder()
                        .nome("teste")
                        .build()
        );

        Mockito.when(clienteRepository.findAll())
                .thenReturn(clientes);

        Assertions.assertEquals(clientes,clienteService.listar());

        Mockito.verify(clienteRepository).findAll();
    }

    @Test
    public void deveCadastrarUmCliente(){
        var cliente = Cliente.builder()
                .nome("teste")
                .build();


        Mockito.when(clienteRepository.save(cliente))
                .thenReturn(cliente);

        Assertions.assertEquals(cliente,clienteService.cadastrar(cliente));

        Mockito.verify(clienteRepository).save(cliente);
    }

    @Test
    public void deveBuscarPedidoPorId(){
        Pedido pedido= Pedido.builder().codigo("123").build();

        Mockito.when(pedidoRepository.findByCodigo("123"))
                .thenReturn(Optional.ofNullable(pedido));

        Assertions.assertEquals(pedido,clienteService.buscarPorId("123"));

        Mockito.verify(pedidoRepository).findByCodigo("123");

    }
}
