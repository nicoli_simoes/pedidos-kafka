package com.kayque.pedidoskafka.kafka;

import lombok.NoArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@NoArgsConstructor
public class KafkaUtil {
    public static Consumer<String, String> createConsumerAndSubcribe(EmbeddedKafkaBroker broker, String topic) {
        Map<String, Object> configs = new HashMap<>(KafkaTestUtils.consumerProps("consumer", "false", broker));
        configs.put("auto.offset.reset", "earliest");
        Consumer<String, String> consumer = new KafkaConsumer<>(configs, new StringDeserializer(), new StringDeserializer());
        consumer.subscribe(Collections.singleton(topic));
        return consumer;
    }
}
