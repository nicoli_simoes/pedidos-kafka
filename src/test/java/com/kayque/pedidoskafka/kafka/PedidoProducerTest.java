package com.kayque.pedidoskafka.kafka;


import com.google.gson.Gson;
import com.kayque.pedidoskafka.entities.Cliente;
import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.entities.Produto;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
@DirtiesContext
public class PedidoProducerTest {

    @Autowired
    private PedidoKafkaProducer pedidoKafkaProducer;

    @Autowired
    private EmbeddedKafkaBroker embeddedKafka;

    @Value("${spring.kafka.topic-json.name}")
    private String topic;

    @Test
    public void deveEnviarMensagemParaOTopico() throws Exception {

        pedidoKafkaProducer.sendMessage(geraPedido());

        var jsonPedido= new Gson().toJson(geraPedido());

       var pedidoRetornado= buscaRecordNoKafka();

       Assertions.assertEquals(jsonPedido,pedidoRetornado);
    }
    private String buscaRecordNoKafka(){
        Consumer<String,String> consumer= KafkaUtil.createConsumerAndSubcribe(embeddedKafka,topic);
        ConsumerRecord<String, String> myRecord = KafkaTestUtils.getSingleRecord(consumer, topic);
        return myRecord.value();
    }

    public Pedido geraPedido(){
        var cliente = Cliente.builder()
                .codigo("codigo")
                .nome("teste")
                .build();

        List<Produto> produtos= Arrays.asList(
                Produto.builder()
                        ._id("123")
                        .nome("produto")
                        .build(),
                Produto.builder()
                        ._id("222")
                        .nome("produto2")
                        .build()
        );

        return Pedido.builder()
                .codigo("codigo")
                .cliente(cliente)
                .produtos(produtos)
                .build();
    }

}
