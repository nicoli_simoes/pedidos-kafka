package com.kayque.pedidoskafka.controllers;

import com.google.gson.Gson;
import com.kayque.pedidoskafka.entities.Pedido;
import com.kayque.pedidoskafka.entities.Produto;
import com.kayque.pedidoskafka.kafka.PedidoKafkaProducer;
import com.kayque.pedidoskafka.services.ClienteService;
import com.kayque.pedidoskafka.entities.Cliente;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PedidoKafkaProducer pedidoKafkaProducer;

    @MockBean
    private ClienteService clienteService;

    @Test
    public void deveRetornar200AoListarClientes() throws Exception {

        List<Cliente> clientes= Arrays.asList(
                Cliente.builder()
                        .nome("abc")
                        .build(),
                Cliente.builder()
                        .nome("teste")
                        .build()
        );

        Mockito.when(clienteService.listar())
                .thenReturn(clientes);

        mockMvc.perform(get("/api/v1/clientes/listar"))
                        .andExpect(status().isOk())
                                .andExpect(jsonPath("$.[0].nome").value("abc"))
                                .andExpect(jsonPath("$.[1].nome").value("teste"));

        Mockito.verify(clienteService).listar();
    }

    @Test
    public void deveRetornar201AoCriarUsuario() throws Exception {

        var cliente= Cliente.builder()
                .nome("teste")
                .build();

        Mockito.when(clienteService.cadastrar(cliente))
                .thenReturn(cliente);

        var json= new Gson().toJson(cliente);

        mockMvc.perform(post("/api/v1/clientes/cadastrar")
                        .contentType("application/json")
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome").value("teste"));

        Mockito.verify(clienteService).cadastrar(cliente);
    }

    @Test
    public void deveRetornar201AoFazerPedido() throws Exception {

        var cliente = Cliente.builder()
                .nome("teste")
                .build();

        List<Produto> produtos= Arrays.asList(
                Produto.builder()
                        .nome("produto")
                        .build(),
                Produto.builder()
                        .nome("produto2")
                        .build()
        );

        var pedido= Pedido.builder()
                .cliente(cliente)
                .produtos(produtos)
                .build();

        var json= new Gson().toJson(pedido);

        mockMvc.perform(post("/api/v1/clientes/fazerPedido")
                        .contentType("application/json")
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.cliente.nome").value("teste"))
                .andExpect(jsonPath("$.produtos.[0].nome").value("produto"))
                .andExpect(jsonPath("$.produtos.[1].nome").value("produto2"));

        Mockito.verify(pedidoKafkaProducer).sendMessage(pedido);
    }

    @Test
    public void deveRetornar201AoBuscarPorId() throws Exception{
        Pedido pedido= Pedido.builder()
                .codigo("123").build();

        Mockito.when(clienteService.buscarPorId("123")).thenReturn(pedido);

        var json= new Gson().toJson(pedido);

        mockMvc.perform(get("/api/v1/clientes/buscaPedido/{codigo}","123"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.codigo").value("123"));

        Mockito.verify(clienteService).buscarPorId("123");

    }
}
