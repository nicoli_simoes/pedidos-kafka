package com.kayque.pedidoskafka.controllers;

import com.google.gson.Gson;
import com.kayque.pedidoskafka.entities.Produto;
import com.kayque.pedidoskafka.services.ProdutoService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ProdutoController.class)
public class ProdutoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProdutoService produtoService;
    @Test
    public void deveCadastrarProduto() throws Exception {

        Produto produto = new Produto(null,"Banana");

        Mockito.when(produtoService.cadastrar(produto)).thenReturn(produto);

        String json = new Gson().toJson(produto);
        mockMvc.perform(post("/produtos/cadastrar").contentType("application/json").content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome").value("Banana"));
    }

    @Test
    public void DeveRetornarTodosOsProdutos() throws Exception{
        List<Produto> produtos= Arrays.asList(
                Produto.builder()
                        .nome("Banana")
                        .build(),
                Produto.builder()
                        .nome("Laranja")
                        .build()
        );

        Mockito.when(produtoService.listarTodos()).thenReturn(produtos);


        mockMvc.perform(get("/produtos")).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].nome").value("Banana"))
                .andExpect(jsonPath("$.[1].nome").value("Laranja"));

        Mockito.verify(produtoService).listarTodos();
    }
}
